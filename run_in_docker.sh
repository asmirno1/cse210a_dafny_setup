img_name="dfy_img"
container_name="dfy_cont"
docker build -t "$img_name" .devcontainer
docker stop "$container_name" && docker rm "$container_name"
docker run -it --name "$container_name" -v "$(pwd)/src":/workspaces/src "$img_name"